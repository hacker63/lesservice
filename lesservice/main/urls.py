"""lesservice URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from . import views as MAIN_VIEWS

urlpatterns = [
    path('', MAIN_VIEWS.index,name='index'),
    path(r'robots.txt',MAIN_VIEWS.robots, name='robots'),
    path('production', MAIN_VIEWS.production, name='production'),
    path('services', MAIN_VIEWS.services, name='services'),
    path('contacts', MAIN_VIEWS.contacts, name='contacts')
]
