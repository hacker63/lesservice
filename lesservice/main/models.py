from django.db import models

# Create your models here.
class ProductionCategory(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=100)
    description = models.TextField(max_length=800)
    prev_image = models.ImageField(upload_to='screenshots/%Y/%m/%d')
    def __str__(self):
        return self.name
    class Meta():
        verbose_name = ' Категория продукции'
        verbose_name_plural = 'Категории продукции'

class Production(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=100)
    description = models.TextField(max_length=800)
    category = models.ForeignKey(ProductionCategory, on_delete=models.CASCADE, blank=True, default=None, null=True)
    prev_image = models.ImageField(upload_to='screenshots/%Y/%m/%d')
    def __str__(self):
        return self.name
    class Meta():
        verbose_name = 'Продукт'
        verbose_name_plural = 'Продукция'

class OptionalService(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=100)
    description = models.TextField(max_length=800)
    prev_image = models.ImageField(upload_to='screenshots/%Y/%m/%d')
    def __str__(self):
        return self.name
    class Meta():
        verbose_name = 'Дополнительная услуга'
        verbose_name_plural = 'Дополнительная услуга'