from django.shortcuts import render, render_to_response
from . import models

# View robots.txt
def robots(request):
    return render_to_response('robots.txt', content_type="text/plain")

# Create your views here.
def index(request):
    context = {
        'title': 'ООО «Лессервис» | Главная страница',
        'META_DESCRIPTION': 'Продажа деревянных поддонов различных типов, размеров и состояния. Доставка до места назначения! Выкуп ваших поддонов с последующим вывозом!',
        'META_KEYWORDS': 'купить поддоны, купить бу поддоны, продать бу поддоны, продать поддоны, доставка поддонов'
    }
    categoryProduction = models.ProductionCategory.objects.all().prefetch_related('production_set')
    context['categoryProduction'] = categoryProduction
    services = models.OptionalService.objects.all()
    context['services'] = services
    return render(request,'index.html',context)

def production(request):
    context = {
        'title': 'ООО «Лессервис» | Продукция',
        'META_DESCRIPTION': 'Мы предлагаем Вам широкий выбор продукции из древесины. Для Вас мы изготовим поддоны на заказ. Или приобретите поддоны стандартизированных размеров.',
        'META_KEYWORDS': 'купить европоддон, купить деревянный поддон, купить поддон, купить типовой поддон, купить усиленный поддон, купить облегченный поддон, купить паллетные борта, купить пиломатериалы, купить паллетные рамки'
    }
    categoryProduction = models.ProductionCategory.objects.all().prefetch_related('production_set')
    context['categoryProduction'] = categoryProduction
    return render(request,'production.html',context)

def services(request):
    context = {
        'title': 'ООО «Лессервис» | Дополнительные услуги',
        'META_DESCRIPTION': 'Мы заинтересованы в выкупе Ваших поддонов. Также поможем Вам избавиться от древесного лома. Еще у нас можно приобрести лофтовую мебель из поддонов.',
        'META_KEYWORDS': 'ремонт поддонов, вывоз поддонов, утилизация поддонов, выкуп поддонов, выкуп бу поддонов, продать бу поддоны, продать поддоны, купить мебель из поддонов'
    }
    services = models.OptionalService.objects.all()
    context['services'] = services
    return render(request,'services.html',context)

def contacts(request):
    context = {
        'title': 'ООО «Лессервис» | Контактные данные',
        'META_DESCRIPTION': 'Для приобретения нашей продукции свяжитесь с нами по телефону или электронной почте.'
    }
    return render(request,'contacts.html',context)