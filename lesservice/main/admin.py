from django.contrib import admin
from . import models

# Register your models here.
admin.site.register(models.ProductionCategory)
admin.site.register(models.Production)
admin.site.register(models.OptionalService)