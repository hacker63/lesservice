create user lesservice_user with password '123qweASD';
alter role lesservice_user set client_encoding to 'utf8';
alter role lesservice_user set default_transaction_isolation to 'read committed';
alter role lesservice_user set timezone to 'UTC';
CREATE DATABASE lesservice_db OWNER lesservice_user;